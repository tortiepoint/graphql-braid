package com.atlassian.braid.source

import com.atlassian.braid.Link
import com.atlassian.braid.SchemaNamespace
import com.atlassian.braid.SchemaSource
import com.atlassian.braid.TestUtil
import graphql.execution.ExecutionContext
import graphql.language.AstPrinter
import graphql.language.Field
import graphql.language.FragmentDefinition
import graphql.language.OperationDefinition
import graphql.schema.DataFetchingEnvironment
import graphql.schema.GraphQLType
import org.junit.Test

import static com.atlassian.braid.TestUtil.parseQuery
import static com.atlassian.braid.TestUtil.typeRegistry
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertTrue
import static org.mockito.Mockito.mock
import static org.mockito.Mockito.when

class TrimFieldsSelectionTest {

    def overallSchemaRegistry = typeRegistry("""
            type Query {
                foo: Foo
                topbar(topBarId: ID) : Bar
            }
            type Foo {
              name: String
              bar: Bar
            }
            type Bar {
              myId: ID
              title: String
            }
        """)

    def overallSchema = TestUtil.schema(overallSchemaRegistry)

    def innerSchema1 = typeRegistry("""
             type Query {
                  topbar(topBarId: String) : Bar
              }
              type Bar {
                  myId: ID
                  title: String
              }
        """)

    // not used at the moment, but provided for claritiy and future tests
    def innerSchema2 = typeRegistry("""
          type Query {
              foo: Foo
          }
          type Foo {
              name: String
              barId: String
          }
        """)


    @Test
    void "trimming root query with one link"() {
        def schemaSource = mock(SchemaSource)
        def environment = mock(DataFetchingEnvironment)

        def namespace1 = SchemaNamespace.of("namespace1")
        def namespace2 = SchemaNamespace.of("namespace2")

        def link = Link
                .from(namespace1, "Foo", "bar", "barId")
                .to(namespace2, "Bar", "topbar", "topBarId")
                .build()

        when(schemaSource.getLinks()).thenReturn(Arrays.asList(link))
        when(schemaSource.getSchema()).thenReturn(innerSchema1)

        when(environment.getGraphQLSchema()).thenReturn(overallSchema)
        when(environment.getParentType()).thenReturn(overallSchema.getType("Query") as GraphQLType)
        def context = mock(ExecutionContext)
        when(environment.getExecutionContext()).thenReturn(context)
        when(context.getVariables()).thenReturn(new LinkedHashMap<String, Object>())


        def query = parseQuery("{foo{bar{myId}}}")
        def rootField = (query.definitions[0] as OperationDefinition).selectionSet.selections[0] as Field

        TrimFieldsSelection.trimFieldSelection(schemaSource, environment, rootField)
        String expectedQuery = "foo { barId }"
        assertEquals(expectedQuery, AstPrinter.printAstCompact(rootField))
    }

    @Test
    void "trim returns cloned fragments bases on the modified query"() {
        def schemaSource = mock(SchemaSource)
        def environment = mock(DataFetchingEnvironment)

        def namespace1 = SchemaNamespace.of("namespace1")
        def namespace2 = SchemaNamespace.of("namespace2")

        def link = Link
                .from(namespace1, "Foo", "bar", "barId")
                .to(namespace2, "Bar", "topbar", "topBarId")
                .build()

        when(schemaSource.getLinks()).thenReturn(Arrays.asList(link))
        when(schemaSource.getSchema()).thenReturn(innerSchema1)

        when(environment.getGraphQLSchema()).thenReturn(overallSchema)
        when(environment.getParentType()).thenReturn(overallSchema.getType("Query") as GraphQLType)
        def context = mock(ExecutionContext)
        when(environment.getExecutionContext()).thenReturn(context)
        when(context.getVariables()).thenReturn(new LinkedHashMap<String, Object>())


        def query = parseQuery("""
            { foo { ...FooFragment  } }
            
            fragment BarFragment on Bar {
                myId
            }                
            fragment FooFragment on Foo {
                bar {
                ...BarFragment
                }
            }
        """)
        def barFragment = (query.definitions[1] as FragmentDefinition)
        def fooFragment = (query.definitions[2] as FragmentDefinition)
        when(environment.getFragmentsByName()).thenReturn([BarFragment: barFragment, FooFragment: fooFragment])
        def rootField = (query.definitions[0] as OperationDefinition).selectionSet.selections[0] as Field

        def referencedFragments = TrimFieldsSelection.trimFieldSelection(schemaSource, environment, rootField)
        assertEquals(1, referencedFragments.size())
        assertEquals(fooFragment.name, referencedFragments.get(0).name)
        assertTrue(fooFragment != referencedFragments.get(0))

    }


    @Test
    void "__typename field on union type special handling"() {
        def overallSchemaRegistry = typeRegistry("""
             type Query {
                  unionField: XorY
              }
              union XorY = X | Y
              type X{
                id: ID
              }
              type Y{
                id: ID
              }
        """)

        def overallSchema = TestUtil.schema(overallSchemaRegistry)

        def innerSchema1 = typeRegistry("""
             type Query {
                  unionField: XorY
              }
              union XorY = X | Y
              type X{
                id: ID
              }
              type Y{
                id: ID
              }
        """)
        def schemaSource = mock(SchemaSource)
        def environment = mock(DataFetchingEnvironment)

        when(schemaSource.getLinks()).thenReturn(Collections.emptyList())
        when(schemaSource.getSchema()).thenReturn(innerSchema1)

        when(environment.getGraphQLSchema()).thenReturn(overallSchema)
        when(environment.getParentType()).thenReturn(overallSchema.getType("Query") as GraphQLType)
        def context = mock(ExecutionContext)
        when(environment.getExecutionContext()).thenReturn(context)
        when(context.getVariables()).thenReturn(new LinkedHashMap<String, Object>())


        def query = parseQuery("{unionField {__typename}}")
        def rootField = (query.definitions[0] as OperationDefinition).selectionSet.selections[0] as Field

        TrimFieldsSelection.trimFieldSelection(schemaSource, environment, rootField)
        String expectedQuery = "unionField { __typename }"
        assertEquals(expectedQuery, AstPrinter.printAstCompact(rootField))

    }


}

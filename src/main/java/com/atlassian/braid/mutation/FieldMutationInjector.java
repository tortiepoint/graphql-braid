package com.atlassian.braid.mutation;

import org.dataloader.BatchLoader;

import java.util.Map;

/**
 * Injects one or more field mutations into the schema during the braiding stage
 */
public interface FieldMutationInjector {

    /**
     * @param braidingContext the context of the schema being braided
     * @return A map of batch loader key and instances to be made available during query execution as data loaders
     */
    Map<String, BatchLoader> inject(BraidingContext braidingContext);
}

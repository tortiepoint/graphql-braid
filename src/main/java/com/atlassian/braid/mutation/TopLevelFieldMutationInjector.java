package com.atlassian.braid.mutation;

import com.atlassian.braid.BraidContext;
import com.atlassian.braid.FieldAlias;
import com.atlassian.braid.SchemaNamespace;
import graphql.execution.DataFetcherResult;
import graphql.language.FieldDefinition;
import graphql.language.ObjectTypeDefinition;
import graphql.language.Type;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.dataloader.BatchLoader;
import org.dataloader.DataLoader;
import org.dataloader.DataLoaderRegistry;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Stream;

import static com.atlassian.braid.mutation.DataFetcherUtils.getDataLoaderKey;
import static com.atlassian.braid.mutation.DataFetcherUtils.getLinkDataLoaderKey;
import static graphql.schema.DataFetchingEnvironmentBuilder.newDataFetchingEnvironment;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonMap;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;


public class TopLevelFieldMutationInjector implements FieldMutationInjector {
    @Override
    public Map<String, BatchLoader> inject(BraidingContext braidingContext) {
        Map<SchemaNamespace, BraidSchemaSource> dataSources = braidingContext.getDataSources();
        List<FieldDataLoaderRegistration> queryLoaders = addSchemaSourcesTopLevelFieldsToOperation(dataSources, braidingContext.getQueryObjectTypeDefinition(),
                BraidSchemaSource::getQueryType, BraidSchemaSource::getQueryFieldAlias);
        List<FieldDataLoaderRegistration> mutationLoaders = addSchemaSourcesTopLevelFieldsToOperation(dataSources, braidingContext.getMutationObjectTypeDefinition(),
                BraidSchemaSource::getMutationType, BraidSchemaSource::getMutationFieldAliases);

        return Stream.of(queryLoaders, mutationLoaders)
                .flatMap(Collection::stream)
                .map(r -> {
                    String dataLoaderKey = getDataLoaderKey(r.type, r.field);
                    braidingContext.getRuntimeWiringBuilder().type(r.type, wiring -> wiring.dataFetcher(r.field, new TopLevelDataFetcher(r.type, r.field)));
                    return singletonMap(dataLoaderKey, r.loader);
                })
                .map(Map::entrySet)
                .flatMap(Collection::stream)
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private static List<FieldDataLoaderRegistration> addSchemaSourcesTopLevelFieldsToOperation
            (Map<SchemaNamespace, BraidSchemaSource> dataSources,
             ObjectTypeDefinition braidOperationType,
             Function<BraidSchemaSource, Optional<ObjectTypeDefinition>> findOperationType,
             BiFunction<BraidSchemaSource, String, Optional<FieldAlias>> getFieldAlias) {
        return dataSources.values()
                .stream()
                .map(source -> addSchemaSourceTopLevelFieldsToOperation(source, braidOperationType, findOperationType, getFieldAlias))
                .flatMap(Collection::stream)
                .collect(toList());
    }

    private static List<FieldDataLoaderRegistration> addSchemaSourceTopLevelFieldsToOperation(
            BraidSchemaSource source,
            ObjectTypeDefinition braidOperationType,
            Function<BraidSchemaSource, Optional<ObjectTypeDefinition>> findOperationType,
            BiFunction<BraidSchemaSource, String, Optional<FieldAlias>> getFieldAlias) {

        return findOperationType.apply(source)
                .map(operationType -> addSchemaSourceTopLevelFieldsToOperation(source, braidOperationType, operationType, getFieldAlias))
                .orElse(emptyList());
    }

    private static List<FieldDataLoaderRegistration> addSchemaSourceTopLevelFieldsToOperation(
            BraidSchemaSource schemaSource,
            ObjectTypeDefinition braidOperationType,
            ObjectTypeDefinition sourceOperationType,
            BiFunction<BraidSchemaSource, String, Optional<FieldAlias>> getFieldAlias) {

        // todo: smarter merge, optional namespacing, etc
        final List<BraidFieldDefinition> fieldDefinitions = aliasedFieldDefinitions(schemaSource, sourceOperationType, getFieldAlias);

        final List<FieldDefinition> braidOperationTypeFieldDefinitions = braidOperationType.getFieldDefinitions();
        fieldDefinitions.forEach(bfd -> braidOperationTypeFieldDefinitions.add(bfd.definition));

        return wireOperationFields(braidOperationType.getName(), schemaSource, fieldDefinitions);
    }

    private static List<BraidFieldDefinition> aliasedFieldDefinitions(BraidSchemaSource schemaSource, ObjectTypeDefinition sourceOperationType, BiFunction<BraidSchemaSource, String, Optional<FieldAlias>> getFieldAlias) {
        return sourceOperationType.getFieldDefinitions().stream()
                .map(definition -> getFieldAlias.apply(schemaSource, definition.getName()).map(alias -> new BraidFieldDefinition(alias, definition)))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(def -> aliasedFieldDefinition(schemaSource, def))
                .collect(toList());
    }

    private static BraidFieldDefinition aliasedFieldDefinition(BraidSchemaSource schemaSource, BraidFieldDefinition braidFieldDefinition) {
        final FieldDefinition definition = braidFieldDefinition.definition;
        Type aliasedType = schemaSource.aliasType(definition.getType());
        return new BraidFieldDefinition(
                braidFieldDefinition.alias,
                FieldDefinition.newFieldDefinition()
                        .name(braidFieldDefinition.alias.getBraidName())
                        .type(aliasedType)
                        .inputValueDefinitions(schemaSource.aliasInputValueDefinitions(definition.getInputValueDefinitions()))
                        .directives(definition.getDirectives()).build());
    }

    private static List<FieldDataLoaderRegistration> wireOperationFields(String typeName,
                                                                                     BraidSchemaSource schemaSource,
                                                                                     List<BraidFieldDefinition> fieldDefinitions) {
        return fieldDefinitions.stream()
                .map(queryField -> wireOperationField(typeName, schemaSource, queryField))
                .collect(toList());
    }

    private static FieldDataLoaderRegistration wireOperationField(
            String typeName,
            BraidSchemaSource schemaSource,
            BraidFieldDefinition operationField) {

        BatchLoader<DataFetchingEnvironment, DataFetcherResult<Object>> batchLoader =
                schemaSource.getSchemaSource().newBatchLoader(schemaSource.getSchemaSource(), new TopLevelFieldTransformation(operationField.alias));

        return new FieldDataLoaderRegistration(typeName, operationField.alias.getBraidName(), batchLoader);
    }

    private static final class BraidFieldDefinition {
        private final FieldAlias alias;
        private final FieldDefinition definition;

        private BraidFieldDefinition(FieldAlias alias, FieldDefinition definition) {
            this.alias = alias;
            this.definition = definition;
        }
    }

    private static class FieldDataLoaderRegistration {
        private final String type;
        private final String field;
        private final BatchLoader<DataFetchingEnvironment, DataFetcherResult<Object>> loader;

        private FieldDataLoaderRegistration(String type, String field, BatchLoader<DataFetchingEnvironment, DataFetcherResult<Object>> loader) {
            this.type = requireNonNull(type);
            this.field = requireNonNull(field);
            this.loader = requireNonNull(loader);
        }
    }

    private static class TopLevelDataFetcher implements DataFetcher {

        private final String type;
        private final String field;

        TopLevelDataFetcher(String type, String field) {
            this.type = type;
            this.field = field;
        }

        @Override
        public Object get(DataFetchingEnvironment env) {
            final DataLoaderRegistry registry = getDataLoaderRegistry(env);
            final Object loadedValue = registry.getDataLoader(getDataLoaderKey(type, field)).load(env);

            // allows a top level field to also be linked
            return Optional.ofNullable(registry.getDataLoader(getLinkDataLoaderKey(type, field)))
                    .map(l -> loadFromLinkLoader(env, loadedValue, l))
                    .orElse(loadedValue);
        }

        private static Object loadFromLinkLoader(DataFetchingEnvironment env,
                                                 Object source,
                                                 DataLoader<Object, Object> dataLoader) {
            return dataLoader.load(newDataFetchingEnvironment(env)
                    .source(source)
                    .fieldDefinition(env.getFieldDefinition())
                    .build());
        }

        static DataLoaderRegistry getDataLoaderRegistry(DataFetchingEnvironment env) {
            return getContext(env).getDataLoaderRegistry();
        }

        static BraidContext getContext(DataFetchingEnvironment env) {
            return env.getContext();
        }
    }


}

package com.atlassian.braid.mutation;

import com.atlassian.braid.FieldKey;
import com.atlassian.braid.FieldMutationContext;
import com.atlassian.braid.FieldTransformation;
import com.atlassian.braid.Link;
import com.atlassian.braid.SchemaSource;
import com.atlassian.braid.java.util.BraidObjects;
import graphql.language.Argument;
import graphql.language.Field;
import graphql.language.InputValueDefinition;
import graphql.language.OperationDefinition;
import graphql.language.Selection;
import graphql.language.Type;
import graphql.language.VariableDefinition;
import graphql.language.VariableReference;
import graphql.schema.DataFetchingEnvironment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static com.atlassian.braid.BatchLoaderUtils.getTargetIdsFromEnvironment;
import static com.atlassian.braid.TypeUtils.findQueryFieldDefinitions;
import static com.atlassian.braid.mutation.QueryMutationUtils.addFieldToQuery;
import static com.atlassian.braid.mutation.QueryMutationUtils.cloneField;
import static com.atlassian.braid.mutation.QueryMutationUtils.getOperationDefinition;
import static java.util.Collections.singletonList;
import static java.util.Objects.requireNonNull;


/**
 * A field mutation that processes a link to a target data source and generates fields for to fetch from that source
 */
public class LinkTransformation implements FieldTransformation {

    private final Link link;

    LinkTransformation(Link link) {
        this.link = requireNonNull(link);
    }

    public Link getLink() {
        return link;
    }

    @Override
    public CompletableFuture<List<Field>> apply(DataFetchingEnvironment environment, FieldMutationContext context) {
        List<Integer> usedCounterIds = new ArrayList<>();

        final OperationDefinition operationDefinition = getOperationDefinition(environment);

        return getTargetIdsFromEnvironment(link, environment)
                .thenApply(targetIds -> {
                    List<Field> fields = new ArrayList<>();
                    Field cloneOfCurrentField = environment.getField().deepCopy();
                    boolean fieldQueryOnlySelectingVariable = isFieldQueryOnlySelectingVariable(cloneOfCurrentField, link);
                    for (Object targetId : targetIds) {
                        final FieldWithCounter field = cloneField(
                                context,
                                usedCounterIds,
                                environment);
                        if (isTargetIdNullAndCannotQueryLinkWithNull(targetId, link)) {
                            context.getShortCircuitedData().put(new FieldKey(field.field.getAlias()), null);
                        } else if (fieldQueryOnlySelectingVariable) {
                            context.getShortCircuitedData().put(new FieldKey(field.field.getAlias()), new HashMap<String, Object>() {{
                                put(link.getTargetVariableQueryField(), targetId);
                            }});
                        } else {
                            addQueryVariable(
                                    context,
                                    targetId,
                                    field);
                            addFieldToQuery(context, environment, operationDefinition, field);
                        }


                        fields.add(field.field);
                    }
                    return fields;
                });
    }

    private void addQueryVariable(FieldMutationContext fieldMutationContext, Object targetId, FieldWithCounter field) {
        final String variableName = link.getArgumentName() + fieldMutationContext.getCounter();

        field.field.setName(link.getTargetQueryField());
        field.field.setArguments(linkQueryArgumentAsList(link, variableName));

        fieldMutationContext.getQueryOp().getVariableDefinitions().add(linkQueryVariableDefinition(link, variableName,
                fieldMutationContext.getSchemaSource() ));
        fieldMutationContext.getVariables().put(variableName, targetId);
    }

    private static List<Argument> linkQueryArgumentAsList(Link link, String variableName) {
        return singletonList(new Argument(link.getArgumentName(), new VariableReference(variableName)));
    }

    private static VariableDefinition linkQueryVariableDefinition(Link link, String variableName, SchemaSource schemaSource) {
        return new VariableDefinition(variableName, findArgumentType(schemaSource, link));
    }

    private static Type findArgumentType(SchemaSource schemaSource, Link link) {
        return findQueryFieldDefinitions(schemaSource.getPrivateSchema())
                .orElseThrow(IllegalStateException::new)
                .stream()
                .filter(f -> f.getName().equals(link.getTargetQueryField()))
                .findFirst()
                .map(f -> f.getInputValueDefinitions().stream()
                        .filter(iv -> iv.getName().equals(link.getArgumentName()))
                        .findFirst()
                        .map(InputValueDefinition::getType)
                        .orElseThrow(IllegalArgumentException::new))
                .orElseThrow(IllegalArgumentException::new);
    }

    private static boolean isTargetIdNullAndCannotQueryLinkWithNull(Object targetId, Link link) {
        return targetId == null && !link.isNullable();
    }

    private static boolean isFieldQueryOnlySelectingVariable(Field field, Link link) {
        final List<Selection> selections = field.getSelectionSet().getSelections();
        return selections.stream().allMatch(s -> s instanceof Field) &&// this means that any fragment will make this return false
                selections.stream()
                        .map(BraidObjects::<Field>cast)
                        .allMatch(f -> f.getName().equals(link.getTargetVariableQueryField()));
    }


}

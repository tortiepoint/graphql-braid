package com.atlassian.braid.mutation;

import com.atlassian.braid.Extension;
import com.atlassian.braid.FieldAlias;
import com.atlassian.braid.Link;
import com.atlassian.braid.SchemaNamespace;
import com.atlassian.braid.SchemaSource;
import com.atlassian.braid.TypeAlias;
import graphql.language.FieldDefinition;
import graphql.language.InputValueDefinition;
import graphql.language.ListType;
import graphql.language.NonNullType;
import graphql.language.ObjectTypeDefinition;
import graphql.language.Type;
import graphql.language.TypeDefinition;
import graphql.language.TypeName;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.atlassian.braid.TypeUtils.DEFAULT_QUERY_TYPE_NAME;
import static com.atlassian.braid.TypeUtils.findMutationType;
import static com.atlassian.braid.TypeUtils.findQueryType;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

/**
 * This wraps a {@link SchemaSource} to enhance it with helper functions
 */
public final class BraidSchemaSource {
    private static final Logger log = LoggerFactory.getLogger(BraidSchemaSource.class);

    private final SchemaSource schemaSource;

    private final TypeDefinitionRegistry registry;
    private final ObjectTypeDefinition queryType;

    private final ObjectTypeDefinition mutationType;

    public BraidSchemaSource(SchemaSource schemaSource) {
        this.schemaSource = requireNonNull(schemaSource);
        this.registry = schemaSource.getSchema();
        this.queryType = findQueryType(registry).orElse(null);
        this.mutationType = findMutationType(registry).orElse(null);
    }

    public SchemaSource getSchemaSource() {
        return schemaSource;
    }

    public SchemaNamespace getNamespace() {
        return schemaSource.getNamespace();
    }

    List<Extension> getExtensions(String type) {
        return schemaSource.getExtensions().stream().filter(e -> e.getType().equals(type)).collect(toList());
    }

    Optional<TypeAlias> getTypeAlias(String type) {
        return schemaSource.getTypeAliases().stream().filter(a -> a.getSourceName().equals(type)).findFirst();
    }

    public Optional<TypeAlias> getTypeAliasFromAlias(String type) {
        return schemaSource.getTypeAliases().stream().filter(a -> a.getBraidName().equals(type)).findFirst();
    }

    public String getBraidTypeName(String sourceTypeName) {
        return getTypeAlias(sourceTypeName)
                .map(TypeAlias::getBraidName)
                .orElse(sourceTypeName);
    }

    public String getSourceTypeName(String braidTypeName) {
        return getTypeAliasFromAlias(braidTypeName)
                .map(TypeAlias::getSourceName)
                .orElse(braidTypeName);
    }

    Optional<FieldAlias> getQueryFieldAlias(String sourceFieldName) {
        return getFieldAlias(schemaSource.getQueryFieldAliases(), sourceFieldName);
    }

    Optional<FieldAlias> getMutationFieldAliases(String sourceFieldName) {
        return getFieldAlias(schemaSource.getMutationFieldAliases(), sourceFieldName);
    }

    /**
     * Gets the actual source type, accounting for links to query objects that have been renamed when merged into the
     * Braid schema
     */
    public String getLinkBraidSourceType(Link link) {
        return getQueryType().map(originalQueryType -> {
            if (originalQueryType.getName().equals(link.getSourceType())) {
                return DEFAULT_QUERY_TYPE_NAME;
            } else {
                return link.getSourceType();
            }
        }).orElse(link.getSourceType());
    }

    public Collection<BraidTypeDefinition> getNonOperationTypes() {
        return registry.types()
                .values()
                .stream()
                .filter(this::isNotOperationType)
                .map(td -> new BraidTypeDefinition(this, td))
                .collect(toList());
    }

    boolean hasType(String type) {
        return getType(type).isPresent();
    }

    Optional<TypeDefinition> getType(String type) {
        return registry.getType(type);
    }

    Optional<ObjectTypeDefinition> getQueryType() {
        return Optional.ofNullable(queryType);
    }

    Optional<ObjectTypeDefinition> getMutationType() {
        return Optional.ofNullable(mutationType);
    }

    public TypeDefinitionRegistry getTypeRegistry() {
        return registry;
    }

    Type aliasType(Type type) {
        if (type instanceof TypeName) {
            final String typeName = ((TypeName) type).getName();
            TypeAlias alias = getTypeAlias(typeName).orElse(TypeAlias.from(typeName, typeName));
            return new TypeName(alias.getBraidName());
        } else if (type instanceof NonNullType) {
            return new NonNullType(aliasType(((NonNullType) type).getType()));
        } else if (type instanceof ListType) {
            return new ListType(aliasType(((ListType) type).getType()));
        } else {
            // TODO handle all definition types (in a generic enough manner)
            log.error("Definition type : " + type + " not handled correctly for aliases.  Please raise an issue.");
            return type;
        }
    }

    public Type unaliasType(Type type) {
        if (type instanceof TypeName) {
            final String typeName = ((TypeName) type).getName();
            TypeAlias alias = getTypeAliasFromAlias(typeName).orElse(TypeAlias.from(typeName, typeName));
            return new TypeName(alias.getSourceName());
        } else if (type instanceof NonNullType) {
            return new NonNullType(unaliasType(((NonNullType) type).getType()));
        } else if (type instanceof ListType) {
            return new ListType(unaliasType(((ListType) type).getType()));
        } else {
            // TODO handle all definition types (in a generic enough manner)
            log.error("Definition type : " + type + " not handled correctly for aliases.  Please raise an issue.");
            return type;
        }
    }

    List<InputValueDefinition> aliasInputValueDefinitions(List<InputValueDefinition> inputValueDefinitions) {
        return inputValueDefinitions.stream()
                .map(input ->
                        InputValueDefinition.newInputValueDefinition()
                                .name(input.getName())
                                .type(aliasType(input.getType()))
                                .defaultValue(input.getDefaultValue())
                                .directives(input.getDirectives())
                                .build())
                .collect(toList());
    }

    private Optional<FieldAlias> getFieldAlias(List<FieldAlias> aliases, String sourceName) {
        // if no mappings explicit mappings defined then expose all fields
        if (aliases.isEmpty()) {
            return Optional.of(FieldAlias.from(sourceName, sourceName));
        }
        return aliases.stream().filter(a -> a.getSourceName().equals(sourceName)).findFirst();
    }

    private Optional<FieldAlias> getFieldAliasFromAlias(List<FieldAlias> aliases, String braidFieldName) {
        // if no mappings explicit mappings defined then expose all fields
        if (aliases.isEmpty()) {
            return Optional.of(FieldAlias.from(braidFieldName, braidFieldName));
        }
        return aliases.stream().filter(a -> a.getBraidName().equals(braidFieldName)).findFirst();
    }

    private boolean isNotOperationType(TypeDefinition typeDefinition) {
        return !isOperationType(typeDefinition);
    }

    private boolean isOperationType(TypeDefinition typeDefinition) {
        requireNonNull(typeDefinition);
        return Objects.equals(queryType, typeDefinition) || Objects.equals(mutationType, typeDefinition);
    }

    boolean hasTypeAndField(TypeDefinitionRegistry registry, TypeDefinition typeDef, FieldDefinition fieldDef) {
        if (findQueryType(registry).map(qType -> qType == typeDef).orElse(false)) {
            return getFieldAliasFromAlias(schemaSource.getQueryFieldAliases(), fieldDef.getName())
                    .map(alias -> alias.getBraidName().equals(fieldDef.getName()))
                    .orElse(false);
        } else {
            return getType(getSourceTypeName(typeDef.getName())).isPresent();
        }
    }
}

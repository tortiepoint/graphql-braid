package com.atlassian.braid.mutation;

class DataFetcherUtils {

    static String getDataLoaderKey(String sourceType, String sourceField) {
        return sourceType + "." + sourceField;
    }

    static String getLinkDataLoaderKey(String sourceType, String sourceField) {
        return sourceType + "." + sourceField + "-link";
    }
}

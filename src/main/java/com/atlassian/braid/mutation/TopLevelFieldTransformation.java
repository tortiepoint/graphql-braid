package com.atlassian.braid.mutation;

import com.atlassian.braid.FieldAlias;
import com.atlassian.braid.FieldMutationContext;
import com.atlassian.braid.FieldTransformation;
import graphql.language.Field;
import graphql.schema.DataFetchingEnvironment;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static com.atlassian.braid.mutation.QueryMutationUtils.addFieldToQuery;
import static com.atlassian.braid.mutation.QueryMutationUtils.cloneField;
import static com.atlassian.braid.mutation.QueryMutationUtils.getOperationDefinition;
import static java.util.Collections.singletonList;
import static java.util.concurrent.CompletableFuture.completedFuture;


/**
 * A mutation that will fetch the top level field from a remote source, optionally changing the name of the field
 */
public class TopLevelFieldTransformation implements FieldTransformation {

    private final FieldAlias alias;

    TopLevelFieldTransformation(FieldAlias alias) {
        this.alias = alias;
    }

    @Override
    public CompletableFuture<List<Field>> apply(DataFetchingEnvironment environment, FieldMutationContext context) {
        FieldWithCounter field = cloneField(context, new ArrayList<>(), environment);

        if (alias.getBraidName().equals(field.field.getName()) && !alias.getSourceName().equals(field.field.getName())) {
            field.field.setName(alias.getSourceName());
        }
        addFieldToQuery(context, environment, getOperationDefinition(environment), field);

        return completedFuture(singletonList(field.field));
    }
}

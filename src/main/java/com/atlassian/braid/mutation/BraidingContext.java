package com.atlassian.braid.mutation;

import com.atlassian.braid.SchemaNamespace;
import graphql.language.ObjectTypeDefinition;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.TypeDefinitionRegistry;

import java.util.Map;


/**
 * Context information used by {@link FieldMutationInjector} instances when braiding the schema
 */
public class BraidingContext {

    private final Map<SchemaNamespace, BraidSchemaSource> dataSources;
    private final TypeDefinitionRegistry registry;
    private final RuntimeWiring.Builder runtimeWiringBuilder;
    private final ObjectTypeDefinition queryObjectTypeDefinition;
    private final ObjectTypeDefinition mutationObjectTypeDefinition;

    public BraidingContext(Map<SchemaNamespace, BraidSchemaSource> dataSources,
                           TypeDefinitionRegistry registry,
                           RuntimeWiring.Builder runtimeWiringBuilder,
                           ObjectTypeDefinition queryObjectTypeDefinition,
                           ObjectTypeDefinition mutationObjectTypeDefinition) {

        this.dataSources = dataSources;
        this.registry = registry;
        this.runtimeWiringBuilder = runtimeWiringBuilder;
        this.queryObjectTypeDefinition = queryObjectTypeDefinition;
        this.mutationObjectTypeDefinition = mutationObjectTypeDefinition;
    }

    Map<SchemaNamespace, BraidSchemaSource> getDataSources() {
        return dataSources;
    }

    TypeDefinitionRegistry getRegistry() {
        return registry;
    }

    RuntimeWiring.Builder getRuntimeWiringBuilder() {
        return runtimeWiringBuilder;
    }

    ObjectTypeDefinition getQueryObjectTypeDefinition() {
        return queryObjectTypeDefinition;
    }

    ObjectTypeDefinition getMutationObjectTypeDefinition() {
        return mutationObjectTypeDefinition;
    }
}

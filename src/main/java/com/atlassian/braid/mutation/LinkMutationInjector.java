package com.atlassian.braid.mutation;

import com.atlassian.braid.BraidContext;
import com.atlassian.braid.Link;
import com.atlassian.braid.SchemaNamespace;
import graphql.language.FieldDefinition;
import graphql.language.ListType;
import graphql.language.NonNullType;
import graphql.language.ObjectTypeDefinition;
import graphql.language.Type;
import graphql.language.TypeDefinition;
import graphql.language.TypeName;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.dataloader.BatchLoader;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static com.atlassian.braid.TypeUtils.findMutationType;
import static com.atlassian.braid.TypeUtils.findQueryType;
import static com.atlassian.braid.mutation.DataFetcherUtils.getLinkDataLoaderKey;
import static java.lang.String.format;

public class LinkMutationInjector implements FieldMutationInjector {
    @Override
    public Map<String, BatchLoader> inject(BraidingContext braidingContext) {
        return linkTypes(braidingContext.getDataSources(), braidingContext.getQueryObjectTypeDefinition(),
                braidingContext.getMutationObjectTypeDefinition(),
                braidingContext.getRuntimeWiringBuilder());
    }

    private static Map<String, BatchLoader> linkTypes(Map<SchemaNamespace, BraidSchemaSource> sources,
                                                      ObjectTypeDefinition queryObjectTypeDefinition,
                                                      ObjectTypeDefinition mutationObjectTypeDefinition,
                                                      RuntimeWiring.Builder runtimeWiringBuilder) {
        Map<String, BatchLoader> batchLoaders = new HashMap<>();
        for (BraidSchemaSource source : sources.values()) {
            TypeDefinitionRegistry typeRegistry = source.getTypeRegistry();
            final TypeDefinitionRegistry privateTypes = source.getSchemaSource().getPrivateSchema();

            Map<String, TypeDefinition> dsTypes = new HashMap<>(typeRegistry.types());

            for (Link link : source.getSchemaSource().getLinks()) {
                // replace the field's type
                ObjectTypeDefinition typeDefinition = getObjectTypeDefinition(queryObjectTypeDefinition,
                        mutationObjectTypeDefinition, typeRegistry, dsTypes, source.getLinkBraidSourceType(link));

                validateSourceFromFieldExists(link, privateTypes);

                Optional<FieldDefinition> sourceField = typeDefinition.getFieldDefinitions().stream()
                        .filter(d -> d.getName().equals(link.getSourceField()))
                        .findFirst();

                Optional<FieldDefinition> sourceFromField = typeDefinition.getFieldDefinitions()
                        .stream()
                        .filter(Objects::nonNull)
                        .filter(s -> s.getName().equals(link.getSourceFromField()))
                        .findAny();

                if (sourceFromField.isPresent() && link.isReplaceFromField()) {
                    typeDefinition.getFieldDefinitions().remove(sourceFromField.get());
                }

                BraidSchemaSource targetSource = sources.get(link.getTargetNamespace());
                if (targetSource == null) {
                    throw new IllegalArgumentException("Can't find target schema source: " + link.getTargetNamespace());
                }
                if (!targetSource.hasType(link.getTargetType())) {
                    throw new IllegalArgumentException("Can't find target type: " + link.getTargetType());

                }

                Type targetType = new TypeName(link.getTargetType());

                if (!sourceField.isPresent()) {
                    // Add source field to schema if not already there
                    if (sourceFromField.isPresent() && isListType(sourceFromField.get().getType())) {
                        targetType = new ListType(targetType);
                    }
                    FieldDefinition field = new FieldDefinition(link.getSourceField(), targetType);
                    typeDefinition.getFieldDefinitions().add(field);
                } else if (isListType(sourceField.get().getType())) {
                    if (sourceField.get().getType() instanceof NonNullType) {
                        sourceField.get().setType(new NonNullType(new ListType(targetType)));
                    } else {
                        sourceField.get().setType(new ListType(targetType));
                    }
                } else {
                    // Change source field type to the braided type
                    sourceField.get().setType(targetType);
                }

                String type = source.getLinkBraidSourceType(link);
                String field = link.getSourceField();
                String linkDataLoaderKey = getLinkDataLoaderKey(type, field);
                runtimeWiringBuilder.type(type, wiring -> wiring.dataFetcher(field, env ->

                        env.<BraidContext>getContext().getDataLoaderRegistry().getDataLoader(linkDataLoaderKey)
                                .load(env)));

                batchLoaders.put(linkDataLoaderKey, targetSource.getSchemaSource().newBatchLoader(targetSource.getSchemaSource(), new LinkTransformation(link)));
            }
        }
        return batchLoaders;
    }

    private static ObjectTypeDefinition getObjectTypeDefinition(ObjectTypeDefinition queryObjectTypeDefinition,
                                                                ObjectTypeDefinition mutationObjectTypeDefinition,
                                                                TypeDefinitionRegistry typeRegistry,
                                                                Map<String, TypeDefinition> dsTypes,
                                                                String linkSourceType) {
        ObjectTypeDefinition typeDefinition = (ObjectTypeDefinition) dsTypes.get(linkSourceType);
        if (typeDefinition == null && linkSourceType.equals(queryObjectTypeDefinition.getName())) {
            typeDefinition = findQueryType(typeRegistry).orElse(null);
            if (typeDefinition == null && linkSourceType.equals(mutationObjectTypeDefinition.getName())) {
                typeDefinition = findMutationType(typeRegistry).orElse(null);
            }
        }

        if (typeDefinition == null) {
            throw new IllegalArgumentException("Can't find source type: " + linkSourceType);
        }
        return typeDefinition;
    }

    private static void validateSourceFromFieldExists(Link link, TypeDefinitionRegistry privateTypeDefinitionRegistry) {

        ObjectTypeDefinition typeDefinition = privateTypeDefinitionRegistry
                .getType(link.getSourceType(), ObjectTypeDefinition.class)
                .orElseThrow(() -> new IllegalArgumentException(
                        format("Can't find source type '%s' in private schema for link %s",
                                link.getSourceType(), link.getSourceField())));
        //noinspection ResultOfMethodCallIgnored
        typeDefinition.getFieldDefinitions().stream()
                .filter(d -> d.getName().equals(link.getSourceFromField()))
                .findFirst()
                .orElseThrow(() ->
                        new IllegalArgumentException(
                                format("Can't find source from field: %s", link.getSourceFromField())));
    }

    private static boolean isListType(Type type) {
        return type instanceof ListType ||
                (type instanceof NonNullType && ((NonNullType) type).getType() instanceof ListType);
    }
}

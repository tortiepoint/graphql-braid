package com.atlassian.braid.mutation;

import com.atlassian.braid.BraidContext;
import com.atlassian.braid.FieldMutationContext;
import com.atlassian.braid.source.TrimFieldsSelection;
import com.atlassian.braid.source.VariableNamespacingGraphQLQueryVisitor;
import graphql.language.Field;
import graphql.language.FragmentDefinition;
import graphql.language.NodeTraverser;
import graphql.language.NodeVisitor;
import graphql.language.OperationDefinition;
import graphql.schema.DataFetchingEnvironment;

import java.util.List;
import java.util.function.Function;

class QueryMutationUtils {

    static OperationDefinition getOperationDefinition(DataFetchingEnvironment environment) {
        return environment.<BraidContext>getContext().getExecutionContext().getOperationDefinition();
    }

    static FieldWithCounter cloneField(FieldMutationContext fieldMutationContext, List<Integer> usedCounterIds,
                                       DataFetchingEnvironment environment) {
        final Field field = cloneFieldBeingFetchedWithAlias(environment, createFieldAlias(fieldMutationContext.getCounter().incrementAndGet()));
        usedCounterIds.add(fieldMutationContext.getCounter().get());
        List<FragmentDefinition> referencedFragments = TrimFieldsSelection.trimFieldSelection(fieldMutationContext.getSchemaSource(), environment, field);
        return new FieldWithCounter(field, fieldMutationContext.getCounter().get(), referencedFragments);
    }

    static void addFieldToQuery(FieldMutationContext fieldMutationContext,
                                       DataFetchingEnvironment environment,
                                       OperationDefinition operationDefinition,
                                       FieldWithCounter field) {
        final NodeVisitor variableNameSpacer =
                new VariableNamespacingGraphQLQueryVisitor(field.counter, operationDefinition,
                        fieldMutationContext.getVariables(), environment,
                        fieldMutationContext.getQueryOp());
        field.referencedFragments.forEach(d -> {
            NodeTraverser nodeTraverser = new NodeTraverser();
            nodeTraverser.preOrder(variableNameSpacer, d);
            fieldMutationContext.getDocument().getDefinitions().add(d);
        });

        NodeTraverser nodeTraverser = new NodeTraverser();
        nodeTraverser.preOrder(variableNameSpacer, field.field);
        fieldMutationContext.getQueryOp().getSelectionSet().getSelections().add(field.field);
    }

    private static Field cloneFieldBeingFetchedWithAlias(DataFetchingEnvironment environment, Function<Field, String> alias) {
        Field field = environment.getField().deepCopy();
        field.setAlias(alias.apply(field));
        return field;
    }

    private static Function<Field, String> createFieldAlias(int counter) {
        return field -> field.getName() + counter;
    }
}

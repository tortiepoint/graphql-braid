package com.atlassian.braid.graphql.language;

import com.atlassian.braid.FieldMutationContext;
import com.atlassian.braid.mutation.BraidSchemaSource;
import graphql.language.Definition;
import graphql.language.Document;
import graphql.language.FieldDefinition;
import graphql.language.FragmentDefinition;
import graphql.language.ObjectTypeDefinition;
import graphql.language.OperationDefinition;
import graphql.language.TypeName;
import graphql.language.VariableDefinition;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLOutputType;

import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;


/**
 * Mutates the graphql document
 */
public class DocumentMutations {
    public static void removeMissingFieldsIfBraidAndSourceTypeFieldsDiffer(FieldMutationContext context, GraphQLOutputType fieldOutputType) {
        BraidSchemaSource ds = new BraidSchemaSource(context.getSchemaSource());
        ds.getSchemaSource().getPrivateSchema().getType(ds.getSourceTypeName(fieldOutputType.getName()))
                .ifPresent(type -> {
                    if (fieldOutputType instanceof GraphQLObjectType && type instanceof ObjectTypeDefinition) {
                        Set<String> braidFieldNames = ((GraphQLObjectType) fieldOutputType).getFieldDefinitions().stream()
                                .map(GraphQLFieldDefinition::getName)
                                .collect(toSet());
                        Set<String> sourceFieldNames = ((ObjectTypeDefinition) type).getFieldDefinitions().stream().map(FieldDefinition::getName).collect(toSet());
                        if (!sourceFieldNames.equals(braidFieldNames)) {
                            context.addMissingFields(new RemoveUnknownFields(
                                    context.getSchemaSource().getPrivateSchema(),
                                    sourceFieldNames,
                                    type.getName(),
                                    context.getDocument()).execute());
                        }
                    }
                });
    }

    public static void unaliasTypes(BraidSchemaSource braidSchemaSource, Document document) {
        List<Definition> newDefinitions = document.getDefinitions().stream()
                .map(def -> {
                    if (def instanceof OperationDefinition) {
                        OperationDefinition op = (OperationDefinition) def;
                        List<VariableDefinition> newVariableDefinitions = op.getVariableDefinitions().stream().map(var -> new VariableDefinition(
                                var.getName(),
                                braidSchemaSource.unaliasType(var.getType()),
                                var.getDefaultValue()))
                                .collect(toList());
                        return op.transform(builder ->
                                builder.variableDefinitions(newVariableDefinitions)
                        );
                    } else if (def instanceof FragmentDefinition) {
                        FragmentDefinition frag = (FragmentDefinition) def;
                        return frag.transform(builder -> builder.typeCondition((TypeName) braidSchemaSource.unaliasType(frag.getTypeCondition())));
                    } else {
                        return def;
                    }
                })
                .collect(toList());
        document.getDefinitions().clear();
        document.getDefinitions().addAll(newDefinitions);
    }
}

package com.atlassian.braid.graphql.language;

import graphql.language.Field;
import graphql.language.FragmentDefinition;
import graphql.language.Node;
import graphql.language.ObjectTypeDefinition;
import graphql.language.SelectionSet;
import graphql.language.TypeDefinition;
import graphql.schema.idl.TypeDefinitionRegistry;
import graphql.validation.DocumentVisitor;
import graphql.validation.LanguageTraversal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Removes unknown fields for a specific type
 */
public class RemoveUnknownFields implements DocumentVisitor {
    private final DefinitionTraversalContext traversalContext;
    private final String sourceTypeName;
    private final Map<String, Field> removedFields = new HashMap<>();
    private final Node node;
    private final Set<String> originalTypeFieldNames;
    private final Map<String, FragmentDefinition> fragments = new HashMap<>();

    RemoveUnknownFields(TypeDefinitionRegistry sourceSchema,
                               Set<String> originalTypeFieldNames,
                               String sourceTypeName,
                               Node node) {
        this.traversalContext = new DefinitionTraversalContext(sourceSchema);
        this.sourceTypeName = sourceTypeName;
        this.originalTypeFieldNames = originalTypeFieldNames;
        this.node = node;
    }

    List<Field> execute() {
        LanguageTraversal languageTraversal = new LanguageTraversal();
        languageTraversal.traverse(node, this);
        fragments.values().forEach(def -> new LanguageTraversal().traverse(def, this));
        return new ArrayList<>(removedFields.values());
    }

    @Override
    public void enter(Node node, List<Node> path) {
        traversalContext.enter(node, path);
        if (node instanceof Field) {
            checkField(traversalContext.getParentType(), path.isEmpty() ? Optional.empty() : Optional.of((SelectionSet) path.get(path.size() - 1)));
        } else if (node instanceof FragmentDefinition) {
            checkFragment((FragmentDefinition) node);
        }
    }

    private void checkFragment(FragmentDefinition node) {
        this.fragments.put(node.getName(), node);
    }

    private void checkField(TypeDefinition parentType, Optional<SelectionSet> parent) {
        if (parentType instanceof ObjectTypeDefinition && parentType.getName().equals(sourceTypeName)) {
            parent.ifPresent(selectionSet -> new ArrayList<>(selectionSet.getSelections()).stream()
                    .filter(s -> s instanceof Field)
                    .filter(s -> !originalTypeFieldNames.contains(((Field) s).getName()))
                    .filter(s -> !((Field) s).getName().startsWith("__"))
                    .forEach(s -> {
                        selectionSet.getSelections().remove(s);
                        removedFields.put(((Field) s).getName(), (Field) s);
                    }));
        }
    }

    @Override
    public void leave(Node node, List<Node> path) {
        traversalContext.leave(node, path);
    }
}

package com.atlassian.braid;

public interface FieldAlias {

    String getSourceName();

    String getBraidName();

    static FieldAlias from(String source, String target) {
        return new FieldAlias() {
            @Override
            public String getSourceName() {
                return source;
            }

            @Override
            public String getBraidName() {
                return target;
            }
        };
    }
}

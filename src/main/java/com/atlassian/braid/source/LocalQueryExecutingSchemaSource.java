package com.atlassian.braid.source;

import com.atlassian.braid.Extension;
import com.atlassian.braid.FieldAlias;
import com.atlassian.braid.Link;
import com.atlassian.braid.SchemaNamespace;
import com.atlassian.braid.SchemaSource;
import com.atlassian.braid.TypeAlias;
import com.atlassian.braid.document.DocumentMapper;
import com.atlassian.braid.document.DocumentMapperFactory;
import com.atlassian.braid.document.DocumentMappers;
import graphql.ExecutionInput;
import graphql.execution.DataFetcherResult;
import graphql.schema.idl.TypeDefinitionRegistry;

import java.io.Reader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static com.atlassian.braid.java.util.BraidObjects.cast;
import static com.atlassian.braid.source.SchemaUtils.loadPublicSchema;
import static com.atlassian.braid.source.SchemaUtils.loadSchema;
import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;
import static java.util.concurrent.CompletableFuture.completedFuture;

/**
 * Local schema source
 */
@SuppressWarnings("WeakerAccess")
public final class LocalQueryExecutingSchemaSource extends ForwardingSchemaSource implements QueryExecutorSchemaSource {
    private final BaseQueryExecutorSchemaSource delegate;
    private final Function<ExecutionInput, Object> queryExecutor;

    private LocalQueryExecutingSchemaSource(SchemaNamespace namespace,
                                            TypeDefinitionRegistry publicSchema,
                                            TypeDefinitionRegistry privateSchema,
                                            Function<ExecutionInput, Object> queryExecutor,
                                            List<Link> links,
                                            List<Extension> extensions,
                                            DocumentMapperFactory documentMapper,
                                            List<TypeAlias> typeAliases,
                                            List<FieldAlias> queryFieldAliases,
                                            List<FieldAlias> mutationFieldAliases) {
        this.queryExecutor = requireNonNull(queryExecutor);
        this.delegate = new BaseQueryExecutorSchemaSource<>(namespace,
                publicSchema,
                privateSchema,
                links,
                extensions,
                documentMapper,
                this::query,
                typeAliases,
                queryFieldAliases,
                mutationFieldAliases);

    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    protected SchemaSource getDelegate() {
        return delegate;
    }

    @Override
    public DocumentMapper getDocumentMapper() {
        return delegate.getDocumentMapper();
    }

    private <C> CompletableFuture<DataFetcherResult<Map<String, Object>>> query(ExecutionInput executionInput, C context) {
        final Object result = queryExecutor.apply(transformExecutionInput(executionInput, context));
        if (result instanceof DataFetcherResult) {
            return completedFuture((cast(result)));
        } else if (result instanceof Map) {
            return completedFuture(new DataFetcherResult<>(cast(result), emptyList()));
        } else {
            CompletableFuture<DataFetcherResult<Map<String, Object>>> future = new CompletableFuture<>();
            future.completeExceptionally(new IllegalStateException("Unexpected result type: " + nullSafeGetClass(result)));
            return future;
        }
    }

    private <C> ExecutionInput transformExecutionInput(ExecutionInput executionInput, C context) {
        return executionInput.transform(builder -> builder.context(context));
    }

    private static Class<?> nullSafeGetClass(Object result) {
        return Optional.ofNullable(result).map(Object::getClass).orElse(null);
    }

    public static class Builder {

        private List<Link> links = emptyList();
        private List<Extension> extensions = emptyList();
        private Supplier<Reader> schemaProvider;
        private SchemaNamespace schemaNamespace;
        private DocumentMapperFactory documentMapperFactory = DocumentMappers.identity();
        private Function<ExecutionInput, Object> remoteRetriever;
        private List<FieldAlias> queryFieldAliases = emptyList();
        private List<FieldAlias> mutationFieldAliases = emptyList();
        private List<TypeAlias> typeAliases = emptyList();

        private Builder() {
        }

        public Builder schemaProvider(Supplier<Reader> schemaProvider) {
            this.schemaProvider = schemaProvider;
            return this;
        }

        public Builder queryFieldAliases(List<FieldAlias> fieldAliases) {
            this.queryFieldAliases = fieldAliases;
            return this;
        }

        public Builder mutationFieldAliases(List<FieldAlias> fieldAliases) {
            this.mutationFieldAliases = fieldAliases;
            return this;
        }

        public Builder topLevelFields(String... topLevelFields) {
            return queryFieldAliases(Arrays.stream(topLevelFields)
                    .map(name -> FieldAlias.from(name, name))
                    .collect(Collectors.toList()));
        }

        public Builder namespace(SchemaNamespace schemaNamespace) {
            this.schemaNamespace = schemaNamespace;
            return this;
        }

        public Builder links(List<Link> links) {
            this.links = links;
            return this;
        }

        public Builder extensions(List<Extension> extensions) {
            this.extensions = extensions;
            return this;
        }

        public Builder documentMapperFactory(DocumentMapperFactory documentMapperFactory) {
            this.documentMapperFactory = documentMapperFactory;
            return this;
        }

        public Builder remoteRetriever(Function<ExecutionInput, Object> remoteRetriever) {
            this.remoteRetriever = remoteRetriever;
            return this;
        }

        public Builder typeAliases(List<TypeAlias> typeAliases) {
            this.typeAliases = typeAliases;
            return this;
        }

        public LocalQueryExecutingSchemaSource build() {
            return new LocalQueryExecutingSchemaSource(
                    requireNonNull(schemaNamespace),
                    loadPublicSchema(
                            requireNonNull(schemaProvider),
                            requireNonNull(links),
                            queryFieldAliases.stream().map(FieldAlias::getSourceName).toArray(String[]::new)),
                    loadSchema(requireNonNull(schemaProvider)),
                    requireNonNull(remoteRetriever),
                    requireNonNull(links),
                    requireNonNull(extensions),
                    requireNonNull(documentMapperFactory),
                    requireNonNull(typeAliases),
                    requireNonNull(queryFieldAliases),
                    requireNonNull(mutationFieldAliases));
        }
    }
}

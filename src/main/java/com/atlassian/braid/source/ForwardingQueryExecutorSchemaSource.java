package com.atlassian.braid.source;

import com.atlassian.braid.document.DocumentMapper;

abstract class ForwardingQueryExecutorSchemaSource extends ForwardingSchemaSource implements QueryExecutorSchemaSource {

    @Override
    protected abstract QueryExecutorSchemaSource getDelegate();

    @Override
    public DocumentMapper getDocumentMapper() {
        return getDelegate().getDocumentMapper();
    }
}

package com.atlassian.braid.source;

import com.atlassian.braid.Link;
import com.atlassian.braid.SchemaSource;
import com.atlassian.braid.mutation.BraidSchemaSource;
import graphql.analysis.QueryTraversal;
import graphql.analysis.QueryVisitor;
import graphql.analysis.QueryVisitorFieldEnvironment;
import graphql.analysis.QueryVisitorStub;
import graphql.language.Field;
import graphql.language.FragmentDefinition;
import graphql.language.FragmentSpread;
import graphql.language.Node;
import graphql.language.NodeTraverser;
import graphql.language.NodeVisitorStub;
import graphql.language.Selection;
import graphql.language.SelectionSet;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLFieldsContainer;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLOutputType;
import graphql.util.TraversalControl;
import graphql.util.TraverserContext;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static graphql.analysis.QueryTraversal.newQueryTraversal;

public class TrimFieldsSelection {


    public static List<FragmentDefinition> trimFieldSelection(SchemaSource schemaSource, DataFetchingEnvironment environment, Node root) {
        List<FieldWithLink> topLevelFieldsWithLinks = new ArrayList<>();
        List<FieldWithLink> fieldsWithLink = new ArrayList<>();
        Map<SelectionSet, List<Field>> fieldsToRemove = new LinkedHashMap<>();
        Map<SelectionSet, List<Field>> fieldsToAdd = new LinkedHashMap<>();

        BraidSchemaSource braidSchemaSource = new BraidSchemaSource(schemaSource);

        QueryVisitor nodeVisitor = new QueryVisitorStub() {

            @Override
            public void visitField(QueryVisitorFieldEnvironment env) {
                if (env.isTypeNameIntrospectionField()) {
                    return;
                }
                GraphQLFieldsContainer parentFieldsContainer = env.getFieldsContainer();
                Field field = env.getField();
                boolean isTopLevelField = environment.getSource() == null;
                if (isTopLevelField) {
                    Optional<Link> linkWithDifferentFromField = getLinkWithDifferentFromField(
                            braidSchemaSource, schemaSource.getLinks(), parentFieldsContainer.getName(), field.getName());
                    if (linkWithDifferentFromField.isPresent()) {
                        topLevelFieldsWithLinks.add(new FieldWithLink(field, linkWithDifferentFromField.get()));
                        return;
                    }
                }
                Optional<Link> link = getLinkWithFieldAsSource(schemaSource.getLinks(), parentFieldsContainer.getName(), field.getName());
                if (link.isPresent()) {
                    fieldsWithLink.add(new FieldWithLink(field, link.get()));
                    return;
                }
                GraphQLFieldDefinition fieldDefinition = parentFieldsContainer.getFieldDefinition(field.getName());
                GraphQLOutputType fieldOutputType = fieldDefinition.getType();
                checkSelectionSet(field.getSelectionSet(), fieldOutputType);

            }


            public void checkSelectionSet(final SelectionSet selectionSet, GraphQLOutputType parentType) {
                if (selectionSet == null) {
                    return;
                }
                if (!selectionSet.getChildren().isEmpty()) {
                    for (final Node child : selectionSet.getChildren()) {

                        // process child to handle cases where the source from root is different than the source root
                        if (child instanceof Field) {
                            Optional<Link> linkWithDifferentFromField = getLinkWithDifferentFromField(
                                    braidSchemaSource, schemaSource.getLinks(), parentType.getName(), ((Field) child).getName());
                            if (linkWithDifferentFromField.isPresent()) {
                                removeSourceFieldIfDifferentThanFromField(selectionSet, linkWithDifferentFromField.get());
                                addFromFieldToQueryIfMissing(selectionSet, linkWithDifferentFromField.get());
                            }
                        }
                    }
                }
            }

            private void addFromFieldToQueryIfMissing(SelectionSet selectionSet, Link link) {
                Optional<Selection> fromField = selectionSet.getSelections().stream()
                        .filter(s -> s instanceof Field
                                && ((Field) s).getName().equals(link.getSourceFromField()))
                        .findFirst();
                if (!fromField.isPresent()) {
                    fieldsToAdd.computeIfAbsent(selectionSet, k -> new ArrayList<>());
                    fieldsToAdd.get(selectionSet).add(new Field(link.getSourceFromField()));
                }
            }

            private void removeSourceFieldIfDifferentThanFromField(SelectionSet selectionSet, Link link) {
                selectionSet.getSelections().stream()
                        .filter(s -> s instanceof Field
                                && ((Field) s).getName().equals(link.getSourceField()))
                        .findAny()
                        .ifPresent(s -> {
                            fieldsToRemove.computeIfAbsent(selectionSet, k -> new ArrayList<>());
                            fieldsToRemove.get(selectionSet).add((Field) s);
                        });
            }
        };

        Map<String, FragmentDefinition> fragmentsByName = environment.getFragmentsByName().entrySet()
                .stream().collect(Collectors.toMap(Entry::getKey, entry -> entry.getValue().deepCopy()));


        QueryTraversal queryTraversal = newQueryTraversal()
                .schema(environment.getGraphQLSchema())
                .root(root)
                .rootParentType((GraphQLObjectType) environment.getParentType())
                .fragmentsByName(fragmentsByName)
                .variables(environment.getExecutionContext().getVariables()).build();
        queryTraversal.visitPreOrder(nodeVisitor);


        fieldsWithLink.forEach(fieldWithLink -> fieldWithLink.field.setSelectionSet(null));
        topLevelFieldsWithLinks.forEach(fieldWithLink -> {
            fieldWithLink.field.setSelectionSet(null);
            fieldWithLink.field.setName(fieldWithLink.link.getSourceFromField());
        });
        fieldsToRemove.forEach((selectionSet, fields) -> selectionSet.getSelections().removeAll(fields));
        fieldsToAdd.forEach((selectionSet, fields) -> selectionSet.getSelections().addAll(fields));


        return getReferencedFragments(root, fragmentsByName);

    }

    private static List<FragmentDefinition> getReferencedFragments(Node root, Map<String, FragmentDefinition> fragmentDefinitionMap) {
        Set<FragmentDefinition> referencedFragments = new LinkedHashSet<>();
        NodeVisitorStub nodeVisitorStub = new NodeVisitorStub() {
            @Override
            public TraversalControl visitFragmentSpread(FragmentSpread fragmentSpread, TraverserContext<Node> context) {
                referencedFragments.add(fragmentDefinitionMap.get(fragmentSpread.getName()));
                return TraversalControl.CONTINUE;
            }
        };
        NodeTraverser nodeTraverser = new NodeTraverser();
        nodeTraverser.preOrder(nodeVisitorStub, root);
        return new ArrayList<>(referencedFragments);
    }

    private static class FieldWithLink {
        public Field field;
        public Link link;

        public FieldWithLink(Field field, Link link) {
            this.field = field;
            this.link = link;
        }
    }

    private static Optional<Link> getLinkWithDifferentFromField(BraidSchemaSource braidSchemaSource, Collection<Link> links, String typeName, String fieldName) {
        return links.stream()
                .filter(l -> braidSchemaSource.getLinkBraidSourceType(l).equals(typeName)
                        && l.getSourceField().equals(fieldName)
                        && !l.getSourceFromField().equals(fieldName))
                .findFirst();
    }

    private static Optional<Link> getLinkWithFieldAsSource(Collection<Link> links, String typeName, String fieldName) {
        return links.stream()
                .filter(l -> l.getSourceType().equals(typeName))
                .filter(l -> l.getSourceFromField().equals(fieldName))
                .findFirst();
    }

}


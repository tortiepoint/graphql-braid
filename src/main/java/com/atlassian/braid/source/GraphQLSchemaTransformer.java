package com.atlassian.braid.source;

import graphql.language.Argument;
import graphql.language.Directive;
import graphql.language.Document;
import graphql.language.EnumTypeDefinition;
import graphql.language.EnumValueDefinition;
import graphql.language.FieldDefinition;
import graphql.language.InputObjectTypeDefinition;
import graphql.language.InputValueDefinition;
import graphql.language.InterfaceTypeDefinition;
import graphql.language.ListType;
import graphql.language.Node;
import graphql.language.NonNullType;
import graphql.language.ObjectTypeDefinition;
import graphql.language.OperationTypeDefinition;
import graphql.language.ScalarTypeDefinition;
import graphql.language.SchemaDefinition;
import graphql.language.Type;
import graphql.language.TypeName;
import graphql.language.UnionTypeDefinition;
import graphql.language.Value;

import java.util.List;
import java.util.stream.Collectors;

/**
 * A visitor for GraphQL schema {@link Document} related objects.
 */
@SuppressWarnings("WeakerAccess")
class GraphQLSchemaTransformer {
    @SuppressWarnings({"unchecked", "StatementWithEmptyBody"})
    <T extends Node> T visit(final T node) {
        T result = null;
        if (node instanceof Document) {
            result = (T) visitDocument((Document) node);
        } else if (node instanceof SchemaDefinition) {
            result = (T) visitSchemaDefinition((SchemaDefinition) node);
        } else if (node instanceof OperationTypeDefinition) {
            result = (T) visitOperationTypeDefinition((OperationTypeDefinition) node);
        } else if (node instanceof ObjectTypeDefinition) {
            result = (T) visitObjectTypeDefinition((ObjectTypeDefinition) node);
        } else if (node instanceof FieldDefinition) {
            result = (T) visitFieldDefinition((FieldDefinition) node);
        } else if (node instanceof EnumTypeDefinition) {
            result = (T) visitEnumTypeDefinition((EnumTypeDefinition) node);
        } else if (node instanceof EnumValueDefinition) {
            result = (T) visitEnumValueDefinition((EnumValueDefinition) node);
        } else if (node instanceof ScalarTypeDefinition) {
            result = (T) visitScalarTypeDefinition((ScalarTypeDefinition) node);
        } else if (node instanceof UnionTypeDefinition) {
            result = (T) visitUnionTypeDefinition((UnionTypeDefinition) node);
        } else if (node instanceof InterfaceTypeDefinition) {
            result = (T) visitInterfaceTypeDefinition((InterfaceTypeDefinition) node);
        } else if (node instanceof InputObjectTypeDefinition) {
            result = (T) visitInputObjectTypeDefinition((InputObjectTypeDefinition) node);
        } else if (node instanceof InputValueDefinition) {
            result = (T) visitInputValueDefinition((InputValueDefinition) node);
        } else if (node instanceof Directive) {
            result = (T) visitDirective((Directive) node);
        } else if (node instanceof Argument) {
            result = (T) visitArgument((Argument) node);
        } else if (node instanceof Value) {
            result = (T) visitValue((Value) node);
        } else if (node instanceof NonNullType) {
            result = (T) visitNonNullType((NonNullType) node);
        } else if (node instanceof ListType) {
            result = (T) visitListType((ListType) node);
        } else if (node instanceof TypeName) {
            result = (T) visitTypeName((TypeName) node);
        } else if (node != null) {
            throw new RuntimeException("Unknown type of node " + node.getClass().getSimpleName() + " at: " + node.getSourceLocation());
        } else {
            // node is null, ignore
        }
        return result;
    }

    private InputValueDefinition visitInputValueDefinition(InputValueDefinition node) {
        return InputValueDefinition.newInputValueDefinition()
                .name(node.getName())
                .type(visitType(node.getType()))
                .defaultValue(visitValue(node.getDefaultValue()))
                .directives(visitNodes(node.getDirectives()))
                .build();
    }

    protected TypeName visitTypeName(TypeName node) {
        return node;
    }

    protected ListType visitListType(ListType node) {
        return new ListType(visit(node));
    }

    protected NonNullType visitNonNullType(NonNullType node) {
        return new NonNullType(visit(node));
    }

    protected ScalarTypeDefinition visitScalarTypeDefinition(ScalarTypeDefinition node) {
        return ScalarTypeDefinition.newScalarTypeDefinition()
                .name(node.getName())
                .directives(visitNodes(node.getDirectives()))
                .build();
    }

    protected Value visitValue(Value node) {
        return node;
    }

    protected Argument visitArgument(Argument node) {
        return new Argument(
                node.getName(),
                visit(node.getValue())
        );
    }

    protected Directive visitDirective(Directive node) {
        return new Directive(
                node.getName(),
                visitNodes(node.getArguments())
        );
    }

    protected InputObjectTypeDefinition visitInputObjectTypeDefinition(InputObjectTypeDefinition node) {
        return InputObjectTypeDefinition.newInputObjectDefinition()
                .name(node.getName())
                .directives(visitNodes(node.getDirectives()))
                .inputValueDefinitions(visitNodes(node.getInputValueDefinitions()))
                .build();
    }

    protected InterfaceTypeDefinition visitInterfaceTypeDefinition(InterfaceTypeDefinition node) {
        return InterfaceTypeDefinition.newInterfaceTypeDefinition()
                .name(node.getName())
                .definitions(visitNodes(node.getFieldDefinitions()))
                .directives(visitNodes(node.getDirectives()))
                .build();
    }

    protected UnionTypeDefinition visitUnionTypeDefinition(UnionTypeDefinition node) {
        return UnionTypeDefinition.newUnionTypeDefinition()
                .name(node.getName())
                .directives(visitNodes(node.getDirectives()))
                .memberTypes(visitNodes(node.getMemberTypes()))
                .build();
    }

    protected EnumValueDefinition visitEnumValueDefinition(EnumValueDefinition node) {
        return new EnumValueDefinition(
                node.getName(),
                visitNodes(node.getDirectives())
        );
    }

    protected EnumTypeDefinition visitEnumTypeDefinition(EnumTypeDefinition node) {
        return EnumTypeDefinition.newEnumTypeDefinition()
                .name(node.getName())
                .enumValueDefinitions(visitNodes(node.getEnumValueDefinitions()))
                .directives(visitNodes(node.getDirectives()))
                .build();
    }

    protected FieldDefinition visitFieldDefinition(FieldDefinition node) {
        return FieldDefinition.newFieldDefinition()
                .name(node.getName())
                .type(visitType(node.getType()))
                .inputValueDefinitions(visitNodes(node.getInputValueDefinitions()))
                .directives(visitNodes(node.getDirectives()))
                .build();
    }

    protected Type visitType(Type type) {
        return type;
    }

    protected ObjectTypeDefinition visitObjectTypeDefinition(ObjectTypeDefinition node) {
        return ObjectTypeDefinition.newObjectTypeDefinition()
                .name(node.getName())
                .implementz(visitNodes(node.getImplements()))
                .directives(visitNodes(node.getDirectives()))
                .fieldDefinitions(visitNodes(node.getFieldDefinitions()))
                .build();
    }

    protected OperationTypeDefinition visitOperationTypeDefinition(OperationTypeDefinition node) {
        return new OperationTypeDefinition(
                node.getName(),
                visitType(node.getType())
        );
    }

    protected SchemaDefinition visitSchemaDefinition(SchemaDefinition node) {
        return SchemaDefinition.newSchemaDefinition()
                .directives(visitNodes(node.getDirectives()))
                .operationTypeDefinitions(visitNodes(node.getOperationTypeDefinitions()))
                .build();
    }

    protected Document visitDocument(final Document node) {
        return new Document(node.getDefinitions().stream().map(this::visit).collect(Collectors.toList()));
    }

    protected <T extends Node> List<T> visitNodes(List<T> nodes) {
        return nodes.stream().map(this::visit).collect(Collectors.toList());
    }
}

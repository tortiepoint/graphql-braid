package com.atlassian.braid.source;

import com.atlassian.braid.Link;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

import java.io.Reader;
import java.util.List;
import java.util.function.Supplier;

import static com.atlassian.braid.TypeUtils.filterQueryType;

public final class SchemaUtils {

    private SchemaUtils() {
    }

    public static TypeDefinitionRegistry loadPublicSchema(Supplier<Reader> schemaProvider, List<Link> links,
                                                          String... topLevelFields) {
        return filterQueryType(loadSchema(schemaProvider), links, topLevelFields);
    }

    public static TypeDefinitionRegistry loadSchema(Supplier<Reader> schema) {
        SchemaParser parser = new SchemaParser();
        return parser.parse(schema.get());
    }
}

package com.atlassian.braid.source;

import com.atlassian.braid.Extension;
import com.atlassian.braid.FieldAlias;
import com.atlassian.braid.FieldTransformation;
import com.atlassian.braid.Link;
import com.atlassian.braid.SchemaNamespace;
import com.atlassian.braid.SchemaSource;
import com.atlassian.braid.TypeAlias;
import graphql.execution.DataFetcherResult;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.dataloader.BatchLoader;

import java.util.List;

abstract class ForwardingSchemaSource implements SchemaSource {
    protected abstract SchemaSource getDelegate();

    @Override
    public TypeDefinitionRegistry getSchema() {
        return getDelegate().getSchema();
    }

    @Override
    public TypeDefinitionRegistry getPrivateSchema() {
        return getDelegate().getPrivateSchema();
    }

    @Override
    public SchemaNamespace getNamespace() {
        return getDelegate().getNamespace();
    }

    @Override
    public List<Link> getLinks() {
        return getDelegate().getLinks();
    }

    @Override
    public List<Extension> getExtensions() {
        return getDelegate().getExtensions();
    }

    @Override
    public List<TypeAlias> getTypeAliases() {
        return getDelegate().getTypeAliases();
    }

    @Override
    public List<FieldAlias> getQueryFieldAliases() {
        return getDelegate().getQueryFieldAliases();
    }

    @Override
    public List<FieldAlias> getMutationFieldAliases() {
        return getDelegate().getMutationFieldAliases();
    }

    @Override
    public BatchLoader<DataFetchingEnvironment, DataFetcherResult<Object>> newBatchLoader(SchemaSource schemaSource, FieldTransformation fieldTransformation) {
        return getDelegate().newBatchLoader(schemaSource, fieldTransformation);
    }
}

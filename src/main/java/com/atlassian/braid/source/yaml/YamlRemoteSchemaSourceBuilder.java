package com.atlassian.braid.source.yaml;

import com.atlassian.braid.Extension;
import com.atlassian.braid.FieldAlias;
import com.atlassian.braid.Link;
import com.atlassian.braid.SchemaNamespace;
import com.atlassian.braid.TypeAlias;
import com.atlassian.braid.document.DocumentMapperFactory;
import com.atlassian.braid.document.DocumentMappers;
import com.atlassian.braid.java.util.BraidMaps;
import com.atlassian.braid.java.util.BraidObjects;

import java.io.Reader;
import java.io.StringReader;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.stream.Collectors.toList;

public class YamlRemoteSchemaSourceBuilder {

    public static Supplier<Reader> buildSchemaProvider(Map<String, Object> m) {
        return () -> new StringReader((String) m.get("schema"));
    }

    public static SchemaNamespace buildSchemaNamespace(Map<String, Object> m) {
        return SchemaNamespace.of((String) m.get("namespace"));
    }

    public static List<FieldAlias> buildQueryAliases(Map<String, Object> m) {
        return Optional.of(BraidMaps.get(m, "queryFields")
                .orElse(BraidMaps.get(m, "topLevelFields")
                        .orElse(emptyList())))
                .map(YamlRemoteSchemaSourceBuilder::getFieldAliasesFromObject)
                .orElseThrow(IllegalStateException::new);
    }

    public static List<FieldAlias> buildMutationAliases(Map<String, Object> m) {
        return Optional.of(BraidMaps.get(m, "mutationFields")
                .orElse(emptyList()))
                .map(YamlRemoteSchemaSourceBuilder::getFieldAliasesFromObject)
                .orElseThrow(IllegalStateException::new);
    }

    public static List<TypeAlias> buildTypeAliases(Map<String, Object> m) {
        return BraidMaps.get(m, "typeAliases")
                .map(BraidObjects::<Map<String, String>>cast)
                .orElse(emptyMap())
                .entrySet()
                .stream()
                .map(entry -> TypeAlias.from(entry.getKey(), entry.getValue()))
                .collect(toList());
    }

    public static List<Link> buildLinks(Map<String, Object> m) {
        final SchemaNamespace fromNamespace = SchemaNamespace.of(getOrThrow(m, "namespace"));

        return BraidMaps.get(m, "links")
                .map(BraidObjects::<List<Map<String, Map<String, Object>>>>cast)
                .map(links -> buildLinks(fromNamespace, links))
                .orElse(emptyList());
    }

    public static List<Extension> buildExtensions(Map<String, Object> m) {
        return BraidMaps.get(m, "extensions")
                .map(BraidObjects::<List<Map<String, Object>>>cast)
                .map(YamlRemoteSchemaSourceBuilder::buildExtensions)
                .orElse(emptyList());
    }

    private static List<Extension> buildExtensions(List<Map<String, Object>> extensions) {
        return extensions.stream().map(YamlRemoteSchemaSourceBuilder::buildExtension).collect(toList());
    }

    private static Extension buildExtension(Map<String, Object> e) {
        return new Extension(
                getOrThrow(e, "type"),
                getOrThrow(e, "field"),
                buildExtensionBy(getOrThrow(e, "by")));
    }

    private static Extension.By buildExtensionBy(Map<String, Object> by) {
        return new Extension.By(
                SchemaNamespace.of(getOrThrow(by, "namespace")),
                getOrThrow(by, "type"),
                getOrThrow(by, "query"),
                getOrThrow(by, "arg"));
    }

    public static DocumentMapperFactory buildDocumentMapperFactory(Map<String, Object> m) {
        return BraidMaps.get(m, "mapper")
                .map(BraidObjects::<List<Map<String, Object>>>cast)
                .map(DocumentMappers::fromYamlList)
                .orElse(DocumentMappers.identity());
    }

    private static List<FieldAlias> getFieldAliasesFromObject(Object fields) {
        if (fields instanceof List) {
            return ((List<String>) fields).stream()
                    .map(f -> FieldAlias.from(f, f))
                    .collect(toList());
        } else if (fields instanceof Map) {
            return ((Map<String, String>) fields).entrySet().stream()
                    .map(entry -> FieldAlias.from(entry.getKey(), entry.getValue()))
                    .collect(toList());
        } else {
            throw new IllegalArgumentException("Unexpected field type");
        }
    }

    private static List<Link> buildLinks(SchemaNamespace fromNamespace, List<Map<String, Map<String, Object>>> links) {
        return links.stream().map(l -> buildLink(fromNamespace, l)).collect(toList());
    }

    private static Link buildLink(SchemaNamespace fromNamespace, Map<String, Map<String, Object>> linkMap) {

        final Map<String, String> from = getOrThrow(linkMap, "from");
        final Map<String, Object> to = getOrThrow(linkMap, "to");

        Link.LinkBuilder linkBuilder = buildFrom(fromNamespace, from);
        linkBuilder = buildTo(linkBuilder, to);

        if (getReplaceFromField(linkMap)) {
            linkBuilder.replaceFromField();
        }

        BraidMaps.get(to, "argument").map(Object::toString).ifPresent(linkBuilder::argument);
        BraidMaps.get(to, "nullable").map(val -> {
            if (val instanceof Boolean) {
                return (Boolean) val;
            } else {
                return Boolean.valueOf((String) val);
            }
        }).ifPresent(linkBuilder::setNullable);

        return linkBuilder.build();
    }

    private static Link.LinkBuilder buildFrom(SchemaNamespace fromNamespace, Map<String, String> from) {
        final String fromField = getOrThrow(from, "field");
        return Link.from(
                fromNamespace,
                getOrThrow(from, "type"),
                fromField,
                BraidMaps.get(from, "fromField").orElse(fromField));
    }

    private static Link.LinkBuilder buildTo(Link.LinkBuilder builder, Map<String, Object> to) {
        return builder.to(
                SchemaNamespace.of(getOrThrow(to, "namespace")),
                getOrThrow(to, "type"),
                BraidMaps.get(to, "field").map(Objects::toString).orElse(null),
                BraidMaps.get(to, "variableField").map(Objects::toString).orElse(null));
    }

    private static <T> T getOrThrow(Map<String, ?> map, String key) {
        return BraidMaps.get(map, key).map(BraidObjects::<T>cast).orElseThrow(IllegalStateException::new);
    }

    private static boolean getReplaceFromField(Map<String, Map<String, Object>> link) {
        return BraidMaps.get(link.get("from"), "replaceFromField")
                .map(BraidObjects::<Boolean>cast)
                .orElse(false);
    }
}

package com.atlassian.braid.source;

import com.atlassian.braid.Extension;
import com.atlassian.braid.FieldAlias;
import com.atlassian.braid.FieldTransformation;
import com.atlassian.braid.Link;
import com.atlassian.braid.SchemaNamespace;
import com.atlassian.braid.SchemaSource;
import com.atlassian.braid.TypeAlias;
import com.atlassian.braid.document.DocumentMapper;
import com.atlassian.braid.document.DocumentMapperFactory;
import graphql.execution.DataFetcherResult;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.dataloader.BatchLoader;

import java.util.List;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;

final class BaseQueryExecutorSchemaSource<C> extends AbstractSchemaSource implements QueryExecutorSchemaSource {

    private final QueryExecutor<C> queryExecutor;
    private final Function<TypeDefinitionRegistry, DocumentMapper> documentMapper;
    private final List<TypeAlias> typeAliases;
    private final List<FieldAlias> queryFieldAliases;
    private final List<FieldAlias> mutationFieldAliases;

    BaseQueryExecutorSchemaSource(SchemaNamespace namespace,
                                  TypeDefinitionRegistry schema,
                                  TypeDefinitionRegistry privateSchema,
                                  List<Link> links,
                                  List<Extension> extensions,
                                  DocumentMapperFactory documentMapper,
                                  QueryFunction<C> queryFunction,
                                  List<TypeAlias> typeAliases,
                                  List<FieldAlias> queryFieldAliases,
                                  List<FieldAlias> mutationFieldAliases) {
        super(namespace, schema, privateSchema, links, extensions);
        this.queryExecutor = new QueryExecutor<>(queryFunction);
        this.documentMapper = requireNonNull(documentMapper);
        this.typeAliases = typeAliases;
        this.queryFieldAliases = queryFieldAliases;
        this.mutationFieldAliases = mutationFieldAliases;
    }

    @Override
    public BatchLoader<DataFetchingEnvironment, DataFetcherResult<Object>> newBatchLoader(SchemaSource schemaSource, FieldTransformation fieldTransformation) {
        return queryExecutor.newBatchLoader(schemaSource, fieldTransformation);
    }

    @Override
    public List<TypeAlias> getTypeAliases() {
        return typeAliases;
    }

    @Override
    public List<FieldAlias> getQueryFieldAliases() {
        return queryFieldAliases;
    }

    @Override
    public List<FieldAlias> getMutationFieldAliases() {
        return mutationFieldAliases;
    }

    public DocumentMapper getDocumentMapper() {
        return documentMapper.apply(getSchema());
    }
}

package com.atlassian.braid;

public interface TypeAlias {

    String getSourceName();

    String getBraidName();

    static TypeAlias from(String source, String target) {
        return new TypeAlias() {
            @Override
            public String getSourceName() {
                return source;
            }

            @Override
            public String getBraidName() {
                return target;
            }
        };
    }
}
